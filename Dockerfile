FROM fedora:30
LABEL MAINTAINER='Tobias Florek <tob@butter.sh>'
EXPOSE 8384/tcp

VOLUME ['/var/syncthing']

RUN dnf install -y syncthing && dnf clean all

USER 1000

HEALTHCHECK --interval=1m --timeout=10s \
  CMD nc -z localhost 8384

ENV HOME=/var/syncthing
CMD ['/usr/bin/syncthing', \
     '-home', '/var/syncthing/config', \
     '-no-browser', \
     '-gui-address', '0.0.0.0:8384']
